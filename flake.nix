{
  description = "Nix configuration for Trèvol's systems";

  inputs = rec {
    # marti shared settings
    shared.url = "gitlab:xvapx/shared?ref=development";
    # encrypt secrets
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "shared/nixpkgs";
    };
    # automate builds
    hydra.url = "github:NixOS/hydra?ref=bbb09986997fa190fd875e17a07891962db24e4e";
  };

  outputs = { self, agenix, shared, hydra }:
  let
      # nixpkgs lib + marti additions
      inherit (shared) lib;
      # flake git revision
      revision = "trevol-${self.shortRev or "DIRTY"}";
      # paths for nixPath
      paths = {
        nixpkgs = shared.nixpkgs.outPath;
        home-manager = shared.home-manager.outPath;
        dotfiles-marti = shared.dotfiles-marti.outPath;
      };
      # modules to import
      imports = {
        inherit agenix;
        inherit (shared) dotfiles-marti;
      };
      # organization settings
      organization = import ./organization.nix { inherit shared lib revision paths imports; };
      # nixpkgs with applied overlays
      nixpkgsFor = system:
        import shared.nixpkgs {
          inherit system;
          overlays = [
            shared.overlays.default
            (final: prev: { 
              # add agenix binary
              agenix = agenix.packages.${system}.default;
              # add hydra flake version
              hydra = hydra.defaultPackage.${system};
            })
          ];
        };

    in
    rec {

      # Used with `nixos-rebuild build --flake .#<hostname>`
      nixosConfigurations = {
        informatica = import ./machines/informatica.nix {
          inherit lib organization nixpkgsFor;
        };
        servidor = import ./machines/servidor.nix {
          inherit lib organization nixpkgsFor;
        };
      };

      # Hydra jobs
      hydraJobs = {
        # Build the config.system.build.toplevel attribute of every machine in nixosConfigurations.
        machines = lib.mapAttrs ( name: value: value.config.system.build.toplevel ) nixosConfigurations;
      };

    };
}
