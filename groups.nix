# Trèvol groups

{
  users.groups = {
    # tots els usuaris de Trèvol
    trevol = {
      gid = 1990;
      members = [
        "andrea"
        "eduard"
        "enric"
        "eugeni"
        "franki"
        "manolo"
        "marti"
        "montse"
        "trevol"
      ];
    };
    # socis
    # consell rector
    # producció neteja
    # producció missatgeria
  };
}
