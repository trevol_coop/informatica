# Trèvol nix configuration

{ lib, outPaths }:

let

  # generate links of nixPath for flake inputs to /run/current-system/
  gen_links = paths: builtins.attrValues (builtins.mapAttrs
    (name: value:
      "ln -s ${value} $out/${name}"
    )
    paths);

  # generate the nixPath for flake inputs linked to /run/current-system/
  gen_paths = paths: builtins.attrValues (builtins.mapAttrs
    (name: value:
      "${name}=/run/current-system/${name}"
    )
    paths);

in
{
  # link nix paths to /run/current-system/
  system.extraSystemBuilderCmds = builtins.concatStringsSep "\n"
    (gen_links outPaths ++
      # copy current configuration to /run/current-system/configuration
      [ "ln -s ${./.} $out/configuration" ]);

  nix = {
    gc = {
      automatic = true;
      dates = "01:00";
      options = "--delete-older-than 20d";
    };
    # preserve outputs and derivations if a result file still points to them
    # to list result files use: nix-store --gc --print-roots
    extraOptions = ''
      gc-keep-outputs = true
      gc-keep-derivations = true
      auto-optimise-store = true
      tarball-ttl = 0
    '';
    # point nixPath to the links in /run/current-system/
    nixPath = (gen_paths outPaths) ++ [
      "nixpkgs=/run/current-system/nixpkgs"
    ];
  };

}
