# Eugeni user settings

{ user_defaults, dotfiles }:

let

  name = "eugeni";
  fullName = "Eugeni Ibáñez";
  uid = 1030;
  home = "/home/${name}";
  default = user_defaults { inherit name uid home; };

in
{

  # user-specific activations scripts
  system.activationScripts."user-${name}" = default.activation.directories;

  # user specific system settings
  users.users."${name}" = default.general // {
    description = fullName;
    openssh.authorizedKeys.keys = [ ];
  };

  # home-manager user settings
  home-manager.users."${name}" = default.home-manager;

}

