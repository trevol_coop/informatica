# Marti user settings

{ lib, config, user_defaults, dotfiles, localization }:

let

  name = "marti";
  fullName = "Martí Serra";
  uid = 1060;
  home = "/home/${name}";
  email = "informatica@trevol.com";
  default = user_defaults { inherit name uid home; };

  secret = file: {
    owner = name;
    inherit file;
  };

in
{

  # user-specific activations scripts
  system.activationScripts."user-${name}" = default.activation.directories;

  # user specific system settings
  users.users."${name}" = default.general // {
    description = fullName;
    extraGroups = [ "audio" "cdrom" "networkmanager" "scanner" "video" "wheel" "xrdp" ];
    openssh.authorizedKeys.keys = [
      # marti casa
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAK7PSl4xpvZgAD2HstjuuyH0+5hCMV74a/oW7jgrB+a xvapx@xvapx"
      # trevol/informatica
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPyuZr+MtN7eOO9V0gdZ9dwHKPhdwr2+HAK4DIDLllS9 informatica@trevol.com"
      # trevol/servidor
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIALtlVmCZJXR2kjSV09J+tzp9qP8O9jcnOvoYwRAnl3A marti@servidor-linux"
      # trevol/tipsa
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCji91oyx3g1QaLdLkg02SDaV+O+kSkamhYw185o1N+LcFZwjvc3wTP+Kb0cZkieJvlxWm9bb5nKY75J/3u4Yk1uKlsLF1BV/AKltz+6JzBTDjnlTHAoeBACS7X9Ao0BnMagkbJZcz6UUglA8CUiuunIkV6iJ0P7lxIOxLeqae+nM5L4idDuNDF2XGh2H9Beeob8I4eLneT4VOaoi2pLGI7+MqpVeS7dB/d9MDertM26J9TR5uSNMc8TaYsTWoEgTi6C4AftWUjuqGp6OJMN4DiBNKXCgPjr5P6rSkLWdgyuByBss3h9X8mM5wijV7cPOQWQLoAczMeyEhwObVf1dyR tipsa@trevol.com"
    ];
  };

  # User agenix-managed secrets
  age.secrets = {
    marti_smb_servidor = secret ../secrets/marti_smb_servidor.age;
  } // ( if config.networking.hostName == "informatica" then {
    marti_gdrive_id = secret ../secrets/marti_gdrive_id.age;
    marti_gdrive_key = secret ../secrets/marti_gdrive_key.age;
  } else {});

  # home-manager user settings
  home-manager.users."${name}" = lib.recursive_merge [
    default.home-manager
    { home.file = {
      ".bash_locale".text = localization.user.bash.en;
      ".config/plasma-localerc".text = localization.user.plasma.en;
    }; }
  ];

  marti.dotfiles = {
    enable = true;
    user = { inherit name email home uid; full_name = fullName; };
    git = {
      enable = true;
      editor = "nvim";
    };
    neovim = {
      enable = true;
      default_editor = true;
    };
    tmux.enable = true;
    bash = {
      enable = true;
      ssh-agent.start = true;
      virtualization.aliases = true;
      tmux.start = true;
    };
    gdrive = {
      enable = true;
      hosts = [ "informatica" ];
    };
  };

}
