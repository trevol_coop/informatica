# root user settings

{ lib, user_defaults, dotfiles }:

let

  name = "root";
  fullName = "root";
  uid = 0;
  home = "/root";
  default = user_defaults { inherit name uid home; };

in
{

  # home-manager user settings
  home-manager.users."${name}" = {
    # IMPORTANT: check https://rycee.gitlab.io/home-manager/release-notes.html before changing stateVersion
    home.stateVersion = default.home-manager.home.stateVersion;
  };

}
