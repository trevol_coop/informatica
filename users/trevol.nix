# Trevol user settings

{ lib, user_defaults, dotfiles }:

let

  name = "trevol";
  fullName = "Admin Trèvol";
  uid = 1000;
  home = "/home/${name}";
  default = user_defaults { inherit name uid home; };

in
{

  # user-specific activations scripts
  system.activationScripts."user-${name}" = default.activation.directories;

  # user specific system settings
  users.users."${name}" = default.general // {
    description = fullName;
    extraGroups = [ "wheel" ];
    openssh.authorizedKeys.keys = [
      # trevol@informatica
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHviez7Oz1cTegtncGS19nqhYtLw0Skr0wB+qXcLN+kg trevol@informatica"
      # marti@informatica
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAKYHfE3KZnVDcnFTaU9WBASQNZiTqbyajC9z2P63QE9 marti@informatica"
    ];
  };

  # home-manager user settings
  home-manager.users."${name}" = lib.recursive_merge [
    default.home-manager
  ];

}
