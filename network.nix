{ shared }:

rec {

  bostik = {
    production = {
      routers = {
        extern = {
          interfaces.primary = {
            ip = "192.168.56.1";
          };
        };
        intern = {
          interfaces = {
            wan = {
              ip = "192.168.56.6";
              mac = "08:60:6E:21:8D:90";
            };
            primary = {
              ip = "192.168.143.6";
              mac = "08:60:6E:21:8D:90";
            };
            management = {
              ip = "192.168.143.6";
              port = "8443";
            };
          };
        };
      };
      servers = {
        produccio = {
          interfaces.primary.ip = "192.168.143.30";
        };
        comptabilitat = {
          interfaces.primary.ip = "192.168.143.31";
        };
        direcline = {
          interfaces.primary.ip = "192.168.143.32";
        };
        servidor = {
          inherit (defaults) defaultGateway nameservers;
          firewall = defaults.firewall // {
            allowedTCPPorts = [
              80      # nginx
              445     # Samba
              139     # Samba netbios
              631     # CUPS
            ];
            allowedUDPPorts = [
              137     # Samba netbios name service
              138     # Samba netbios datagram service
              139     # Samba netbios session service
              631     # CUPS
            ];
          };
          interfaces = {
            primary = {
              mac = "00:25:22:20:5f:14";
              # bond slaves do not have IP address
            };
            secondary = {
              mac = "00:80:ad:17:19:59";
              # bond slaves do not have IP address
            };
            bond0 = {
              ip = "192.168.143.45";
              mac = "";
              # bridge slaves do not have IP address
            };
            bridge0 = {
              ip = "192.168.143.45";
              mac = "";
              inherit (defaults) prefixLength;
            };
          };
          hostName = "linux";
          # generated with 'head -c4 /dev/urandom | od -A none -t x4'
          hostId = "c4a9c4e2";
        };
      };
      workstations = {
        comercial.interfaces.primary.ip = "192.168.143.50";
        comptabilitat.interfaces.primary.ip = "192.168.143.51";
        administracio.interfaces.primary.ip = "192.168.143.52";
        infres.interfaces.primary.ip = "192.168.143.53";
        neteja-1.interfaces.primary.ip = "192.168.143.54";
        neteja-2.interfaces.primary.ip = "192.168.143.55";
        informatica = {
          inherit (defaults) defaultGateway nameservers firewall;
          interfaces.primary = {
            ip = "192.168.143.59";
            mac = "50:E5:49:EE:88:ED";
            inherit (defaults) prefixLength;
          };
          hostName = "informatica";
          # generated with 'head -c4 /dev/urandom | od -A none -t x4'
          hostId = "80b015d7";
        };
        trafic.interfaces.primary.ip = "192.168.143.60";
        tipsa.interfaces.primary.ip = "192.168.143.61";
        trafic-2.interfaces.primary.ip = "192.168.143.62";
      };
      printers = {
        hp.ip = "192.168.143.20";
        brother.ip = "192.168.143.22";
      };
      services = {
        hydra.ip = bostik.production.servers.servidor.interfaces.bridge0.ip;
        smb.servidor = with bostik.production.servers.servidor; {
          ip = interfaces.bridge0.ip;
          hostname = hostName;
        };
        borgbackup.ip = bostik.production.workstations.informatica.interfaces.primary.ip;
      };
    };
  };

  # default setting for this organization's machines
  defaults = {
    prefixLength = 24;
    defaultGateway = bostik.production.routers.intern.interfaces.primary.ip;
    nameservers = shared.nameservers.cloudflare.ipv4;
    firewall = {
      enable = true;
      allowPing = true;
    };
    nixosBinaryCaches = shared.nixosBinaryCaches ++ [
      "http://${bostik.production.services.hydra.ip}:3000"
    ];
    binaryCachePublicKeys = shared.binaryCachePublicKeys ++ [
      "cache.trevol.com-01:WGNHC8g4honRVhwY2JNAS6aDSgYNZ7tTt1cRf84YhZM="
    ];
    sshPorts = [ 22 ];
  };

}
