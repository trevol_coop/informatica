{ config, lib, pkgs, revision, network, machine, dotfiles }:

{
  environment = {
    variables = {
      EDITOR = "vi";
    };
    systemPackages = with pkgs; [
    ];
  };

  # Machine approximate location
  location = {
    latitude = 41.3945498;
    longitude = 2.1751121;
  };

  system.stateVersion = "22.05";

  # delete all files in /tmp during boot
  boot.cleanTmpDir = true;

  programs = {
    # Allow the system to send emails using sendmail
    msmtp = {
      enable = true;
      setSendmail = true;
      defaults = {
        tls_starttls = false;
        port = 465;
        tls = true;
      };
      accounts = {
        "default" = {
          host = "lin112.loading.es";
          auth = true;
          user = "alertes@trevol.com";
          from = "alertes@trevol.com";
          passwordeval = "cat ${config.age.secrets.alertes_pass.path}";
        };
      };
    };
  };

  # agenix secrets
  age.secrets = let rootSecret = file: {
      owner = "root";
      inherit file;
    }; in {
    # borg repo magatzem passphrase and ssh keys
    borgbackup_magatzem = rootSecret ../secrets/borgbackup_magatzem.age;
    borgbackup_magatzem_ssh = rootSecret ../secrets/borgbackup_magatzem_ssh.age;
    borgbackup_magatzem_ssh_pub = rootSecret ../secrets/borgbackup_magatzem_ssh_pub.age;
    # borg repo contaplus passphrase and keys
    borgbackup_contaplus = rootSecret ../secrets/borgbackup_contaplus.age;
    borgbackup_contaplus_ssh = rootSecret ../secrets/borgbackup_contaplus_ssh.age;
    borgbackup_contaplus_ssh_pub = rootSecret ../secrets/borgbackup_contaplus_ssh_pub.age;
    # borg repo direcline passphrase and keys
    borgbackup_direcline = rootSecret ../secrets/borgbackup_direcline.age;
    borgbackup_direcline_ssh = rootSecret ../secrets/borgbackup_direcline_ssh.age;
    borgbackup_direcline_ssh_pub = rootSecret ../secrets/borgbackup_direcline_ssh_pub.age;
    # alertes@trevol.com password
    alertes_pass = rootSecret ../secrets/alertes_pass.age;
    # hydra nginx auth_basic_user_file
    hydra_nginx_auth = {
      group = "nginx";
      mode = "0440";
      file = ../secrets/hydra_nginx_auth.age;
    };
  };
  
  # machine-specific root scripts
  home-manager.users."root".home.file = {
    "/bin/backup_contaplus_vm" = {
      text = dotfiles.scripts.backup_contaplus_vm;
      executable = true;
    };
    "/bin/backup_direcline_vm" = {
      text = dotfiles.scripts.backup_direcline_vm;
      executable = true;
    };
  };

  services = {

    cron = {
      enable = lib.mkForce true;
      systemCronJobs = [
        # root, contaplus vm backup 22:00
        "0 22 * * * root /root/bin/backup_contaplus_vm"
        # root, direcline vm backup 22:30
        "30 22 * * * root /root/bin/backup_direcline_vm"
      ];
    };

    # Enable printing and printer sharing
    printing = {
      enable = true;
      browsing = true;
      logLevel = "debug";
      allowFrom = [
        "localhost"
        "192.168.143.*"
      ];
      defaultShared = true;
      listenAddresses = [
        "127.0.0.1:631"
        "${machine.interfaces.bridge0.ip}:631"
      ];
      startWhenNeeded = false;
    };

    # Enable avahi printer sharing
    avahi = {
      enable = true;
      ipv4 = true;
      ipv6 = false;
      nssmdns = true;
      publish = {
        enable = true;
        userServices = true;
        addresses = true;
      };
    };

    # Enable samba sharing
    samba = {
      enable = true;
      package = pkgs.sambaFull;
      nsswins = true;
      enableWinbindd = true;
      enableNmbd = true;
      extraConfig = ''
        [global]
        workgroup = GESTIO
        server string = ${machine.hostName}
        netbios name = ${machine.hostName}
        server role = standalone server
        hosts allow = 192.168.143.0/24 localhost
        hosts deny = 0.0.0.0/0
        security = user
        guest account = nobody
        map to guest = Bad Password
        invalid users = root
        load printers = no
        printing = CUPS
        directory mask = 0755
        force create mode = 0644
        force directory mode = 0755
      '';
      shares = {
        MFP-M283fdw = {
          comment = "HP Color Laserjet Pro MFP M283fdw";
          path = "/var/spool/samba";
          printer = "HP-MFP-M283fdw";
          public = "yes";
          browseable = "yes";
          "guest ok" = "yes";
          writable = "no";
          printable = "yes";
          "create mode" = "0700";
        };
        public = {
          comment = "Public samba share";
          path = "/srv/public";
          public = "yes";
          browseable = "yes";
          "guest ok" = "yes";
          writable = "yes";
          "write list" = "@trevol";
        };
        magatzem = {
          comment = "Documents Trevol";
          path = "/srv/magatzem";
          public = "yes";
          browseable = "yes";
          writable = "yes";
          # TODO: fix this once trafic-1 printer is not shared with SMB1
          #"create mask" = "0770";
          #"directory mask" = "2770";
          "guest ok" = "yes";
          #"valid users" = "@trevol";
          #"write list" = "@trevol";
        };
      };
    };

    # backups
    borgbackup = {
      jobs = {
        # manually initialize the borg repo with:
        # sudo borg init --encryption=repokey --rsh "ssh -i /run/agenix/borgbackup_contaplus_ssh" borg@${network.bostik.production.services.borgbackup.ip}:.
        contaplus_local = {
          paths = [ "/mnt/backups/vm/Contaplus" ];
          repo = "borg@${network.bostik.production.services.borgbackup.ip}:.";
          doInit = false;
          encryption = {
            mode = "repokey";
            passCommand = "cat ${config.age.secrets.borgbackup_contaplus.path}";
          };
          environment.BORG_RSH = "ssh -i ${config.age.secrets.borgbackup_contaplus_ssh.path}";
          # for more info, see:
          # borg help compression
          compression = "auto,lzma";
          # format: https://www.freedesktop.org/software/systemd/man/systemd.time.html
          # to check if $format is valid, use:
          # systemd-analyze calendar "$format"
          startAt = "23:00";
          # for more info, see:
          # borg help prune
          prune.keep = {
            # Keep the last 7 daily copies
            daily = 7;
            # Keep the last 4 weekly copies
            weekly = 4;
            # Keep the last 12 monthly copies
            monthly = 12;
            # Keep at least one archive for each year
            yearly = -1;
          };
        };
        # manually initialize the borg repo with:
        # sudo borg init --encryption=repokey --rsh "ssh -i /run/agenix/borgbackup_direcline_ssh" borg@${network.bostik.production.services.borgbackup.ip}:.
        direcline_local = {
          paths = [ "/mnt/backups/vm/Direcline" ];
          repo = "borg@${network.bostik.production.services.borgbackup.ip}:.";
          doInit = false;
          encryption = {
            mode = "repokey";
            passCommand = "cat ${config.age.secrets.borgbackup_direcline.path}";
          };
          environment.BORG_RSH = "ssh -i ${config.age.secrets.borgbackup_direcline_ssh.path}";
          # for more info, see:
          # borg help compression
          compression = "auto,lzma";
          # format: https://www.freedesktop.org/software/systemd/man/systemd.time.html
          # to check if $format is valid, use:
          # systemd-analyze calendar "$format"
          startAt = "23:15";
          # for more info, see:
          # borg help prune
          prune.keep = {
            # Keep the last 7 daily copies
            daily = 7;
            # Keep the last 4 weekly copies
            weekly = 4;
            # Keep the last 12 monthly copies
            monthly = 12;
            # Keep at least one archive for each year
            yearly = -1;
          };
        };
        # manually initialize the borg repo with:
        # sudo borg init --encryption=repokey --rsh "ssh -i /run/agenix/borgbackup_magatzem_ssh" borg@${network.bostik.production.services.borgbackup.ip}:.
        magatzem_local = {
          paths = [ "/srv/magatzem" ];
          exclude = [ "/srv/magatzem/marti/INFORMATICA/backups" ];
          repo = "borg@${network.bostik.production.services.borgbackup.ip}:.";
          doInit = false;
          encryption = {
            mode = "repokey";
            passCommand = "cat ${config.age.secrets.borgbackup_magatzem.path}";
          };
          environment.BORG_RSH = "ssh -i ${config.age.secrets.borgbackup_magatzem_ssh.path}";
          # for more info, see:
          # borg help compression
          compression = "auto,lzma";
          # format: https://www.freedesktop.org/software/systemd/man/systemd.time.html
          # to check if $format is valid, use:
          # systemd-analyze calendar "$format"
          startAt = "23:30";
          # for more info, see:
          # borg help prune
          prune.keep = {
            # Keep the last 7 daily copies
            daily = 7;
            # Keep the last 4 weekly copies
            weekly = 4;
            # Keep the last 12 monthly copies
            monthly = 12;
            # Keep at least one archive for each year
            yearly = -1;
          };
        };
      };
    };

  };

  # software roles
  marti.roles = {
    common = {
      enable = true;
      version_label = revision;
      # hydra sets the correct nix package
      nix_package = null;
    };
    desktop = {
      enable = false;
    };
    server = {
      hydra = {
        enable = true;
        db.manage = true;
        space_queue = 50;
        space_evaluator = 50;
        use_binary_cache = true;
        nix.max_jobs = config.marti.profiles.cpu.cores;
        package = pkgs.hydra;
        mail.address = "informatica@trevol.com";
        web = {
          host = "192.168.143.45";
          url = "https://coop.trevol.com/hydra";
        };
        nginx = {
          enable = true;
          authFile = "${config.age.secrets.hydra_nginx_auth.path}";
        };
      };
      nextcloud = {
        # TODO: enable nextcloud using agenix for the admin password file
        enable = false;
        db.admin_password_file = "";
      };
    };
    virtualization.libvirtd-kvm = {
      enable = true;
      users = [ "marti" ];
    };
  };
  systemd = {
    tmpfiles.rules = [
      # create /var/spool/samba directory
      "d /var/spool/samba 1777 root root -"
    ];
    services = { }
    # generate alert service for each borgbackup job
    // (lib.mapAttrs' (name: value: lib.nameValuePair ("alert-borgbackup-job-${name}") ({
      serviceConfig = {
        User = "root";
      };
      script = ''
        printf "Subject:Error durant borgbackup-job-${name}.\n
        Log desde inici del sistema: \n$(journalctl -b -u borgbackup-job-${name}.service)" \
        | ${pkgs.msmtp}/bin/msmtp informatica@trevol.com
      '';
    }) ) config.services.borgbackup.jobs )
    # make each borgbackup job service start its alert service upon failure
    // (lib.mapAttrs' (name: value: lib.nameValuePair ("borgbackup-job-${name}") ({
      unitConfig.OnFailure = "alert-borgbackup-job-${name}.service";
    })) config.services.borgbackup.jobs );
  };
}
