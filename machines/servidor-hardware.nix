{ network }:
let

  motherboard = {
    # sudo dmidecode | grep -A4 '^Base Board Information'
    vendor = "Asrock";
    model = "M3A785GMH/128M";
    # Either "UEFI" or "BIOS",
    # check for the existence of /sys/firmware/efi
    uefi = false;
    nvram = false;
  };

  cpu = {
    # lscpu
    vendor = "amd";
    model = "AMD Athlon(tm) II X4 635 Processor";
    byte_order = "Little Endian";
    # use 'nproc --all' to determine this number, nixos default is 1.
    cores = 4;
  };

  gpu = {
    # lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1)
    vendor = "amd";
    model = "Advanced Micro Devices, Inc. [AMD/ATI] RS880 [Radeon HD 4200]";
    # Activate OpenGL
    opengl = false;
  };

  network_devices = {
    # sudo lshw -class network | grep -A 1 "bus info" | grep name | awk -F': ' '{print $2}'
    interfaces = {
     # "enp2s0" = network.interfaces.primary;
     # "enp3s5" = network.interfaces.secondary;
     # "bond0" = network.interfaces.bond0;
      "br0" = network.interfaces.bridge0;
    };
    # bond interfaces
    # see https://www.kernel.org/doc/Documentation/networking/bonding.txt
    bonds = {
      bond0 = {
        driverOptions = {
          mode = "balance-xor";
          xmit_hash_policy = "encap2+3";
          miimon = "100";
          downdelay = "200";
          updelay = "200";
        };
        interfaces = [ "enp2s0" "enp3s5" ];
      };
    };
    # bridges
    bridges = {
      # bridge 0, used by kvm
      br0 = {
        interfaces = [ "bond0" ];
      };
    };
  };



  disks = {

    system-1 = {
      # Device info, get with 'sudo hdparm -I /dev/sda'
      device = "/dev/sda";
      vendor = "Western Digital";
      model = "WDC WD30EZRX-00SPEB0";
      serial = "WD-WCC4E3RD0PFC";
      # boot from this device?
      boot = true;
      # spin down this device when not in use?
      spin-down = true;
      # monitor this device with smartd?
      smartd = true;
      smartd-options = "-d sat";
      # Partitions info, get with 'lsblk -o name,mountpoint,label,size,fstype,uuid'
      partitions = {
        raid = {
          filesystem = "raid";
        };
      };
    };

    system-2 = {
      # Device info, get with 'sudo hdparm -I /dev/sda'
      device = "/dev/sdb";
      vendor = "seagate";
      model = "ST2000DM001-1ER164";
      serial = "Z8E09JSN";
      # boot from this device?
      boot = true;
      # spin down this device when not in use?
      spin-down = true;
      # monitor this device with smartd?
      smartd = true;
      smartd-options = "-d sat";
      # Partitions info, get with 'lsblk -o name,mountpoint,label,size,fstype,uuid'
      partitions = {
        raid = {
          filesystem = "raid";
        };
      };
    };

    # sudo blkid, /dev/mapper/{boot,root,swap} devices
    system-raid = {
      partitions = {
        boot = {
          device = "/dev/disk/by-uuid/ae2eba7f-4731-4bed-8e36-cbf833f6c9f4";
          filesystem = "ext2";
          mountpoint = "/boot";
        };
        root = {
          device = "/dev/disk/by-uuid/0ec1d7bb-8ffa-4808-ae4b-051484caf4ee";
          filesystem = "xfs";
          mountpoint = "/";
        };
        swap = {
          device = "/dev/disk/by-uuid/9f28856f-db44-4f68-a1e3-b0b63dbb957a";
          filesystem = "swap";
        };
      };
    };

  };

in
rec {

  marti.profiles = {
    cpu = {
      inherit (cpu) vendor cores;
    };
    gpu = {
      inherit (gpu) vendor;
      openGl.enable = gpu.opengl;
    };
    network = {
      bluetooth.enable = network_devices.bluetooth or false;
      settings = { inherit (network) hostName hostId defaultGateway nameservers firewall; };
      inherit (network_devices) interfaces bonds bridges;
    };
    filesystems = {
      inherit disks;
      boot = {
        # boot raid even if degraded
        preLVMCommands = ''
          mdadm --run /dev/md1
        '';
      };
      # mdadm configuration (RAID), get with 'mdadm --detail --scan'
      mdadm.conf = ''
        DEVICE partitions
        ARRAY /dev/md1 metadata=1.0 UUID=de796404:eb08d18c:b52c1b2a:3e9de5d8
        MAILADDR informatica@trevol.com
      '';
      # Kernel modules
      kernel.modules = [ "ahci" "ohci_pci" "ehci_pci" "ata_piix" "pata_atiixp"
      "pata_jmicron" "firewire_ohci" "usb_storage" "usbhid" "sd_mod" "sr_mod"
      "kvm-amd" "dm-snapshot" "dm-raid" "br_netfilter" ];
    };
  };

}
