{ config, lib, pkgs, revision, network, machine, dotfiles }:

{
  environment = {
    variables = {
      EDITOR = "vi";
      BROWSER = "firefox";
    };
    systemPackages = with pkgs; [
      grive2
      agenix
    ];
  };

  system.stateVersion = "22.05";

  # Machine approximate location
  location = {
    latitude = 41.3945498;
    longitude = 2.1751121;
  };

  # delete all files in /tmp during boot
  boot.cleanTmpDir = true;

  programs = {
    # Allow the system to send emails using sendmail
    msmtp = {
      enable = true;
      setSendmail = true;
      defaults = {
        tls_starttls = false;
        port = 465;
        tls = true;
      };
      accounts = {
        "default" = {
          host = "lin112.loading.es";
          auth = true;
          user = "informatica@trevol.com";
          from = "informatica@trevol.com";
          passwordeval = "cat ${config.age.secrets.informatica_pass.path}";
        };
      };
    };
  };
  
  # agenix secrets
  age.secrets = let rootSecret = file: {
      owner = "root";
      inherit file;
    }; in {
    # borg repo magatzem passphrase
    borgbackup_magatzem = rootSecret ../secrets/borgbackup_magatzem.age;
    # borg repo contaplus passphrase
    borgbackup_contaplus = rootSecret ../secrets/borgbackup_contaplus.age;
    # borg repo direcline passphrase
    borgbackup_direcline = rootSecret ../secrets/borgbackup_direcline.age;
    # informatica@trevol.com password
    informatica_pass = rootSecret ../secrets/informatica_pass.age;
  };

  # machine-specific root scripts
  home-manager.users."root".home.file = {
    "/bin/borg_summary_magatzem" = {
      text = dotfiles.scripts.borg_summary_magatzem {
        passphrase = "$(cat ${config.age.secrets.borgbackup_magatzem.path})"; };
      executable = true;
    };
    "/bin/borg_summary_contaplus" = {
      text = dotfiles.scripts.borg_summary_contaplus {
        passphrase = "$(cat ${config.age.secrets.borgbackup_contaplus.path})"; };
      executable = true;
    };
    "/bin/borg_summary_direcline" = {
      text = dotfiles.scripts.borg_summary_direcline {
        passphrase = "$(cat ${config.age.secrets.borgbackup_direcline.path})"; };
      executable = true;
    };
  };

  services = {
    redshift = {
      enable = true;
      brightness.night = "0.8";
    };
    urxvtd.enable = true;
    printing = {
      enable = true;
      drivers = [ pkgs.hplip ];
    };
    cron = {
      enable = lib.mkForce true;
      systemCronJobs = [
        # root, magatzem borgbackup summary daily at 07:00
        "0 7 * * * root /root/bin/borg_summary_magatzem"
        # root, contaplus borgbackup summary daily at 07:00
        "0 7 * * * root /root/bin/borg_summary_contaplus"
        # root, direcline borgbackup summary daily at 07:00
        "0 7 * * * root /root/bin/borg_summary_direcline"
      ];
    };
    # borgbackup server to store backups from servidor
    borgbackup.repos = {
      magatzem = {
        path = "/var/lib/borgbackup/magatzem";
        # Generate on the machine running borg with:
        # sudo ssh-keygen -N '' -t ed25519 -f /run/keys/id_ed25519_repo_name
        authorizedKeys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN9K3h6GdYfRpPjyPrSlGumiU1UE1y8v2FoyjMBozy4v root@linux"
        ];
      };
      contaplus = {
        path = "/var/lib/borgbackup/contaplus";
        # Generate on the machine running borg with:
        # sudo ssh-keygen -N '' -t ed25519 -f /run/keys/id_ed25519_repo_name
        authorizedKeys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAFEG/bJ1JxJYXJqbCwyPhQ1Ctk7AUVu326wdbMW0fsM root@linux"
        ];
      };
      direcline = {
        path = "/var/lib/borgbackup/direcline";
        # Generate on the machine running borg with:
        # sudo ssh-keygen -N '' -t ed25519 -f /run/keys/id_ed25519_repo_name
        authorizedKeys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINlcmKnv0N1obQf4nSbwRVkuz8mKbPbi4LZM3PtmU/iu root@linux"
        ];
      };
    };
    # Enable samba sharing
    samba = {
      enable = true;
      openFirewall = true;
      nsswins = true;
      enableWinbindd = true;
      enableNmbd = true;
      extraConfig = ''
        [global]
        workgroup = GESTIO
        server string = ${machine.hostName}
        netbios name = ${machine.hostName}
        server role = standalone server
        hosts allow = 192.168.143.0/24 localhost
        hosts deny = 0.0.0.0/0
        security = user
        guest account = nobody
        map to guest = Bad Password
        invalid users = root
        load printers = no
        directory mask = 0755
        force create mode = 0644
        force directory mode = 0755
      '';
      shares = {
        public = {
          comment = "Public samba share";
          path = "/srv/public";
          public = "yes";
          browseable = "yes";
          "guest ok" = "yes";
          writable = "yes";
          "write list" = "@trevol";
        };
      };
    };
  };

  # software roles
  marti.roles = {
    common = {
      enable = true;
      version_label = revision;
    };
    desktop = {
      enable = true;
      kde.enable = true;
      remote = {
        client = true;
        server = {
          enable = true;
        };
        assistance = {
          enable = true;
          language = "en";
        };
      };
    };
    editor = {
      cad.enable = true;
      document = {
        enable = true;
        scan.users = [ "marti" ];
      };
      image.enable = true;
      multimedia.enable = true;
    };
    virtualization.libvirtd-kvm = {
      enable = true;
      users = [ "marti" ];
    };
    flatpak.enable = true;
    social.enable = true;
    developer = {
      enable = true;
      android = {
        enable = true;
        users = [ "marti" ];
      };
      database.enable = true;
      docker.enable = true;
      infosec.enable = true;
      nix.enable = true;
    };
  };
  systemd = {
    mounts = [ {
      what = "//${network.bostik.production.services.smb.servidor.ip}/magatzem";
      where = "/mnt/magatzem";
      type = "cifs";
      options =
        "rw"
        + ",nofail"
        + ",credentials=${config.age.secrets.marti_smb_servidor.path}"
        + ",iocharset=utf8";
    } ];
    automounts = [ {
      where = "/mnt/magatzem";
      wantedBy = [ "multi-user.target" ];
    } ];
  };

}
