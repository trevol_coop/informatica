{ lib, nixpkgsFor, organization }:

let

  # machine network settings
  network = organization.network.bostik.production.workstations.informatica;
  # machine localization settings
  locale = organization.localization.machine.en_GB-es;
  # machine architecture
  system = "x86_64-linux";
  # pkgs collection based on machine architecture
  pkgs = nixpkgsFor system;
  # machine software
  software = { config, ... }: import ./informatica-software.nix {
    inherit lib pkgs config;
    inherit (organization) revision network;
    machine = network;
    dotfiles = organization._dotfiles { inherit pkgs; };
  };
  # machine harware
  hardware = import ./informatica-hardware.nix {
    inherit network;
  };

in

lib.nixosSystem {
  inherit system;
  modules = [
    software
    hardware
    locale
  ] ++ organization.modules;
}
