{ network }:
let

  motherboard = {
    # sudo dmidecode | grep -A4 '^Base Board Information'
    vendor = "gigabyte";
    model = "H61M-DS2";
    # Either "UEFI" or "BIOS",
    # check for the existence of /sys/firmware/efi
    uefi = true;
    nvram = false;
  };

  cpu = {
    # lscpu
    vendor = "intel";
    model = "Intel(R) Pentium(R) CPU G620 @ 2.60GHz";
    byte_order = "Little Endian";
    # use 'nproc --all' to determine this number, nixos default is 1.
    cores = 2;
  };

  gpu = {
    # lspci  -v -s  $(lspci | grep ' VGA ' | cut -d" " -f 1)
    vendor = "intel";
    model = "2nd Generation Core Processor Family Integrated Graphics Controller (rev 09)";
    # Activate OpenGL
    opengl = true;
  };

  network_devices = {
    # sudo lshw -class network | grep -A 1 "bus info" | grep name | awk -F': ' '{print $2}'
    interfaces = {
      "enp2s0" = network.interfaces.primary;
    };
  };

  disks = {

    system = {
      # Device info, get with 'sudo hdparm -I /dev/sda'
      device = "/dev/sda";
      vendor = "western digital";
      model = "WDC WD20EARX-00PASB0";
      serial = "WD-WMAZA8989491";
      # Only without a boot device will grub boot from uefi without a legacy bios_grub partition.
      boot = false;
      spin-down = true;
      smartd = true;
      smartd-options = "-d sat";

      # Partitions info, get with 'lsblk -o name,mountpoint,label,size,fstype,uuid'
      partitions = {
        boot = {
          label = "boot";
          filesystem = "vfat";
          mountpoint = "/boot";
        };
        nixos = {
          label = "nixos";
          filesystem = "ext4";
          mountpoint = "/";
        };
        swap = {
          label = "swap";
          filesystem = "swap";
        };
      };

    };

  };

in
rec {

  marti.profiles = {
    cpu = {
      inherit (cpu) vendor cores;
    };
    gpu = {
      inherit (gpu) vendor;
      openGl.enable = gpu.opengl;
    };
    network = {
      bluetooth.enable = network_devices.bluetooth or false;
      settings = { inherit (network) hostName hostId defaultGateway nameservers firewall; };
      inherit (network_devices) interfaces;
    };
    filesystems = {
      boot = {
        efi = {
          support = motherboard.uefi;
          modifiable = motherboard.nvram;
          removable = !motherboard.nvram;
        };
      };
      inherit disks;
    };
    yubikey.enable = true;
  };

}
