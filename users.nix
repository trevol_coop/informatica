# Trèvol users

{ lib, pkgs, config, dotfiles, localization }:

let

  user_defaults = { name, uid, home }: {

    activation = {
      # create /var/run/${uid} and /nix/var/nix/profiles/per-user/${name}
      directories = ''
        mkdir -m 0700 -p /run/user/${toString uid} /nix/var/nix/profiles/per-user/${name}
        chown ${name}:users /run/user/${toString uid} /nix/var/nix/profiles/per-user/${name}
      '';
    };

    general = {
      inherit uid home;
      isNormalUser = true;
      createHome = true;
      group = "users";
      extraGroups = [ "audio" "cdrom" "networkmanager" "scanner" "video" ];
    };

    home-manager = {
      home = {
        # IMPORTANT: check https://rycee.gitlab.io/home-manager/release-notes.html before changing stateVersion
        stateVersion = "22.05";
        # dotfiles
        file = {
          # avoid KDE errors
          ".cache/kde/.keep".text = "";
          # localization
          ".bash_locale".text = localization.user.bash.ca;
          ".config/plasma-localerc".text = localization.user.plasma.ca;
        };
        sessionVariables = {
          XDG_CACHE_HOME = "${home}/.cache";
          XDG_CONFIG_HOME = "${home}/.config/";
          XDG_DATA_HOME = "${home}/.local/share";
          XDG_STATE_HOME = "${home}/.local/state";
          XDG_RUNTIME_DIR = "/run/user/${toString uid}";          
        };
      };
    };

  };

in
lib.recursive_merge [
  (import ./users/andrea.nix { inherit user_defaults dotfiles; })
  (import ./users/eduard.nix { inherit user_defaults dotfiles; })
  (import ./users/enric.nix { inherit user_defaults dotfiles; })
  (import ./users/eugeni.nix { inherit user_defaults dotfiles; })
  (import ./users/franki.nix { inherit user_defaults dotfiles; })
  (import ./users/manolo.nix { inherit user_defaults dotfiles; })
  (import ./users/marti.nix { inherit lib config user_defaults dotfiles localization; })
  (import ./users/montse.nix { inherit user_defaults dotfiles; })
  (import ./users/trevol.nix { inherit lib user_defaults dotfiles; })
  (import ./users/root.nix { inherit lib user_defaults dotfiles; })
]
