{ pkgs, repository, passphrase, account, email }:

''
  #!/usr/bin/env bash

  set -euo pipefail

  ############ SETTINGS

  # Borg binary
  BORG=${pkgs.borgbackup}/bin/borg

  # Repository
  REPO=${repository}

  # passphrase
  BORG_PASSPHRASE=${passphrase}
  export BORG_PASSPHRASE

  # msmtp account
  ACCOUNT=${account}

  # email recipient
  RECIPIENT=${email}

  ############ SUMMARY

  # Generate borg space info
  space=$($BORG info $REPO | grep "All archives" \
  | awk -F '[[:space:]][[:space:]]+' \
  '{print "Original: " $2 "\nCompressed: " $3 "\nDeduplicated: " $4}')

  # Generate borg list
  list=$($BORG list $REPO)
  last=$(echo "$list" | tail -n1 | awk '{ print $2 " " $3 " " $4 }')
  archives=$(echo "$list" | awk '{ print $1 }')

  # Send email
  printf "Subject:Summary of %s.\n\n\
  ## Last completed backup:\n%s\n\n\
  ## Space usage:\n%s\n\n\
  ## Archive list:\n%s" \
  "$REPO" "$last" "$space" "$archives" \
  | msmtp -a $ACCOUNT $RECIPIENT
''
