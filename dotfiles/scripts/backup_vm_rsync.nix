# trevol vm backup script

{ vm_name
, backups_dir ? "/home/trevol/backups/vm/${vm_name}"
, remote_backups_dir ? "/mnt/magatzem/backups"
, stop_vm ? true
, shutdown_mode ? "acpi" # acpi, agent, initctl, signal or paravirt
, shutdown_wait ? 600 # In seconds
, pkgs
}:

''
  #!/usr/bin/env bash

  set -euo pipefail

  ############ SETTINGS

  # VM name
  VM=${vm_name}

  # Backups directory
  BACKUPS_DIR=${backups_dir}

  # Timestamp
  TIMESTAMP=''$(date +%Y-%m-%d_%H%M)

  # Backup directory
  BACKUP_DIR=''${BACKUPS_DIR}/''${TIMESTAMP}

  # Log file
  LOGFILE=''${BACKUPS_DIR}/''${VM}-''${TIMESTAMP}.log

  # Backup archive
  BACKUP_ARCHIVE=''${BACKUPS_DIR}/''${VM}-''${TIMESTAMP}.tar.xz
  
  # Array to store the logs before the logfile is created
  declare -a logs=("##### ''${TIMESTAMP} - Start of ''${VM} backup.")

  # Logger function
  log () {
    string="## $(date +%H:%M:%S) - ''${1}"
    if [ -z "''${LOGFILE_EXISTS+x}" ]; then
      logs+=("''${string}")
    fi
    printf "%s\n" "''${string}"
  }

  ############ START BACKUP

  log "Creating the backup directory ''${BACKUP_DIR}..."
  mkdir -p "''${BACKUP_DIR}"
  log "Backup directory created."

  # Print the logs to LOGFILE
  LOGFILE_EXISTS=1
  printf "%s\n" "''${logs[@]}" > "''${LOGFILE}"

  # Copy stdout and stderr from here on to LOGFILE
  exec > >(tee -ia "''${LOGFILE}")
  exec 2> >(tee -ia "''${LOGFILE}" >& 2)

  # Backup the machine config
  log "Dumping ''${VM} machine configuration to ''${BACKUP_DIR}/machine.xml..."
  ${pkgs.libvirt}/bin/virsh dumpxml ''${VM} > "''${BACKUP_DIR}/machine.xml"
  log "Machine configuration dumped."

  # Generate VM disks array
  declare -a vm_disks
  while IFS= read -r disk; do
    vm_disks+=( "$disk" )
  done < <(${pkgs.libvirt}/bin/virsh domblklist ''${VM} --details | awk '/disk/{print $3 ":" $4}')

'' + (if stop_vm then ''
  # Shut down the machine
  log "Shutting down ''${VM}..."
  ${pkgs.libvirt}/bin/virsh shutdown ''${VM} --mode ${shutdown_mode}
  # Wait for shutdown or wait_max
  vm_status=
  wait_max=$(( $(date +%s) + ${shutdown_wait} ))
  while [ $(date +%s) -lt $wait_max ]; do
    vm_status=$(${pkgs.libvirt}/bin/virsh list --all | grep " ''${VM} " | awk '{ print $3}')
    if ([ ''${vm_status} == "shut off" ]); then
      break
    else
      sleep 10
    fi
  done
  # Check if the vm has stopped
  if ([ ''${vm_status} != "shut off" ]); then
    log "''${VM} did not shut down gracefully, saving state to file ''${BACKUP_DIR}/memory_dump..."
    ${pkgs.libvirt}/bin/virsh save ''${VM} "''${BACKUP_DIR}/memory_dump"
    vm_status="saved"
    log "Saved memory state."
  else
    log "''${VM} powered off."
  fi

'' else ''
  # Generate a temporary VM snapshot to hold the changes while we backup the disks
  log "Generating temporary snapshot to continue working while the frozen disks are backed up..."
  declare -a diskspec
  for disk in "''${vm_disks[@]}"; do
    device=''${disk%:*}
    diskspec+=(" --diskspec ''${device},file=''${BACKUP_DIR}/tmp-snapshot-''${device}-''${TIMESTAMP}.qcow2")
  done
  ${pkgs.libvirt}/bin/virsh snapshot-create-as \
  --domain ''${VM} --name "tmp-snapshot-''${TIMESTAMP}" --quiesce --disk-only \
  --atomic --no-metadata ''${diskspec[@]}
  log "Snapshot generated."

'') + ''
  # Zero unused space, backup & compress the VM disks
  # disabled until virt-sparsify is available again
  # https://bugs.archlinux.org/task/72591
  #log "Compressing the machine's disks for backup..."
  #for disk in "''${vm_disks[@]}; do
  #  device=''${disk%:*}
  #  file=''${disk#*:}
  #  ${pkgs.libguestfs}/bin/virt-sparsify -q --compress ''${file} ''${BACKUP_DIR}/''${VM}-''${device}-''${TIMESTAMP}.qcow2
  #done
  #log "Disk compression finished."
  # while virt-sparsify is not available, copy the entire disk
  log "Copying the machine's disks..."
  for disk in "''${vm_disks[@]}"; do
    device=''${disk%:*}
    file=''${disk#*:}
    cp "''${file}" "''${BACKUP_DIR}/''${VM}-''${device}-''${TIMESTAMP}.qcow2"
  done
  log "Disks copied."

'' + (if stop_vm then ''
  # Start the machine again
  log "Restarting ''${VM}..."
  if ([ ''${vm_status} == "shut off" ]); then
    ${pkgs.libvirt}/bin/virsh start ''${VM}
    log "''${VM} restarted."
  else
    ${pkgs.libvirt}/bin/virsh restore "''${BACKUP_DIR}/memory_dump"
    log "''${VM} restored."
  fi

'' else ''
  # Merge back and delete the temporary snapshot
  log "Merging back the temporary snapshots..."
  for disk in "''${vm_disks[@]}"; do
    device=''${disk%:*}
    ${pkgs.libvirt}/bin/virsh blockcommit ''${VM} "''${device}" --active --verbose --pivot
    rm -v "''${BACKUP_DIR}/tmp-snapshot-''${device}-''${TIMESTAMP}.qcow2"
  done
  log "Temporary snapshot merged back and deleted."

'') + ''
  # Archive and compress backup
  log "Archiving and compressing backup..."
  XZ_OPT='-T0 -9' ${pkgs.gnutar}/bin/tar -cJf "''${BACKUP_ARCHIVE}" "''${BACKUP_DIR}"
  log "Backup archived and compressed."
  
  # Remove uncompressed backup
  log "Deleting local backup..."
  rm -fr "''${BACKUP_DIR}"
  log "Local backup deleted."

  # TODO: Remove this once the server is pulling the backups
  # Rsync backup to remote server
  log "Copying backup to ${remote_backups_dir}..."
  mkdir -p ${remote_backups_dir}
  ${pkgs.rsync}/bin/rsync -hPa "''${BACKUP_ARCHIVE}" ${remote_backups_dir}
  log "Backup Copied."

  # End log
  log "End of backup."

  # TODO: Remove this once the server is pulling the backups
  # Copy log to remote backup dir
  ${pkgs.rsync}/bin/rsync -q "''${LOGFILE}" ${remote_backups_dir}
''
