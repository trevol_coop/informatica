# trevol vm backup script

{ vm_name
, backup_dir ? "/mnt/backups/vm/${vm_name}"
, stop_vm ? true
, shutdown_mode ? "acpi" # acpi, agent, initctl, signal or paravirt
, shutdown_wait ? 600 # In seconds
, snapshot_attempts ? 2 # times to attempt taking a snapshot to live backup
, pkgs
}:

''
  #!/usr/bin/env bash

  set -euo pipefail

  ############ SETTINGS

  # VM name
  VM=${vm_name}

  # Backups directory
  BACKUP_DIR=${backup_dir}

  # Timestamp
  TIMESTAMP=''$(date +%Y-%m-%d_%H%M)

  # Log file
  LOGFILE=''${BACKUP_DIR}/''${VM}-backup.log

'' + (if (!stop_vm) then ''
  # Snapshot attempts
  SNAPSHOT_ATTEMPTS=${toString snapshot_attempts}

'' else "") + ''
  # Array to store the logs before the logfile is created
  declare -a logs=("##### ''${TIMESTAMP} - Start of ''${VM} backup.")

  # Logger function
  log () {
    string="## $(date +%H:%M:%S) - ''${1}"
    if [ -z "''${LOGFILE_EXISTS+x}" ]; then
      logs+=("''${string}")
    fi
    printf "%s\n" "''${string}"
  }

  ############ START BACKUP

  log "Creating the backup directory ''${BACKUP_DIR}..."
  mkdir -p "''${BACKUP_DIR}"
  log "Backup directory created."

  # Print the logs to LOGFILE
  LOGFILE_EXISTS=1
  printf "%s\n" "''${logs[@]}" > "''${LOGFILE}"

  # Copy stdout and stderr from here on to LOGFILE
  exec > >(tee -ia "''${LOGFILE}")
  exec 2> >(tee -ia "''${LOGFILE}" >& 2)

  # Backup the machine config
  log "Dumping ''${VM} machine configuration to ''${BACKUP_DIR}/machine.xml..."
  ${pkgs.libvirt}/bin/virsh dumpxml ''${VM} > "''${BACKUP_DIR}/machine.xml"
  log "Machine configuration dumped."

  # Generate VM disks array
  declare -a vm_disks
  while IFS= read -r disk; do
    vm_disks+=( "$disk" )
  done < <(${pkgs.libvirt}/bin/virsh domblklist ''${VM} --details | awk '/disk/{print $3 ":" $4}')

'' + (if stop_vm then ''
  # Shut down the machine
  log "Shutting down ''${VM}..."
  ${pkgs.libvirt}/bin/virsh shutdown ''${VM} --mode ${shutdown_mode}
  # If windows has powered off the display, the first shutdown order can wake 
  # the system up instead of shutting it down, so we wait 10 seconds and repeat
  # the shutdown order, which might fail.
  set +e
  sleep 10
  ${pkgs.libvirt}/bin/virsh shutdown ''${VM} --mode ${shutdown_mode}
  set -e
  # Wait for shutdown or wait_max
  vm_status=
  wait_max=$(( $(date +%s) + ${toString shutdown_wait} ))
  while [ $(date +%s) -lt $wait_max ]; do
    vm_status=$(${pkgs.libvirt}/bin/virsh list --all | grep " ''${VM} " | awk -F "   " '{print $3}')
    if ([ "''${vm_status}" = "shut off" ]); then
      break
    else
      log "Waiting for the vm to shut down..."
      sleep 10
    fi
  done
  # Check if the vm has stopped
  if ([ "''${vm_status}" != "shut off" ]); then
    log "''${VM} did not shut down gracefully, saving state to file ''${BACKUP_DIR}/memory_dump..."
    ${pkgs.libvirt}/bin/virsh save ''${VM} "''${BACKUP_DIR}/memory_dump"
    vm_status="saved"
    log "Saved memory state."
  else
    log "''${VM} powered off."
  fi

'' else ''
  # Generate a temporary VM snapshot to hold the changes while we backup the disks
  log "Generating temporary snapshot to continue working while the frozen disks are backed up..."
  declare -a diskspec
  for disk in "''${vm_disks[@]}"; do
    device=''${disk%:*}
    diskspec+=(" --diskspec ''${device},file=''${BACKUP_DIR}/tmp-snapshot-''${device}-''${TIMESTAMP}.qcow2")
  done
  function take_snapshot() {
    ${pkgs.libvirt}/bin/virsh snapshot-create-as \
    --domain ''${VM} --name "tmp-snapshot-''${TIMESTAMP}" --quiesce --disk-only \
    --atomic --no-metadata ''${diskspec[@]}
  }
  # Try to take the snapshot
  attempts=0
  result=1
  set +e
  until [ ''${result} -eq 0 ] || [ ''${attempts} -ge ''${SNAPSHOT_ATTEMPTS} ]; do
    log "Snapshot attempt ''${attempts}:"
    take_snapshot
    result=$?
    ((attempts++))
    sleep 60
  done
  set -e
  if [ ''${result} -eq 0 ] then
    log "Snapshot generated."
  else
    log "Error: Could not generate snapshot."
    exit 1
  fi

'') + ''
  log "Copying the machine's disks..."
  for disk in "''${vm_disks[@]}"; do
    device=''${disk%:*}
    file=''${disk#*:}
    cp -v "''${file}" "''${BACKUP_DIR}/''${VM}-''${device}.qcow2"
  done
  log "Disks copied."

'' + (if stop_vm then ''
  # Start the machine again
  log "Restarting ''${VM}..."
  if ([ "''${vm_status}" == "shut off" ]); then
    ${pkgs.libvirt}/bin/virsh start ''${VM}
    log "''${VM} restarted."
  else
    ${pkgs.libvirt}/bin/virsh restore "''${BACKUP_DIR}/memory_dump"
    log "''${VM} restored."
  fi

'' else ''
  # Merge back and delete the temporary snapshot
  log "Merging back the temporary snapshots..."
  for disk in "''${vm_disks[@]}"; do
    device=''${disk%:*}
    ${pkgs.libvirt}/bin/virsh blockcommit ''${VM} "''${device}" --active --verbose --pivot
    rm -v "''${BACKUP_DIR}/tmp-snapshot-''${device}-''${TIMESTAMP}.qcow2"
  done
  log "Temporary snapshot merged back and deleted."

'') + ''
  # End log
  log "End of backup."
''
