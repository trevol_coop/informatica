# Trèvol parametrized ssh/config

{ extra-pre ? ""
, extra-post ? ""
, home ? "~"
, auto-add ? false
}:

# TODO: make the address depend on the network configuration SSOT
extra-pre + ''
  Host servidor
    HostName 192.168.143.30
    Port 22
    IdentityFile ${home}/.ssh/trevol
''
+ (if auto-add then ''
  AddKeysToAgent yes

'' else "")
  + extra-post

