{ pkgs, lib }:

{

  scripts = {

    # Contaplus vm backup
    backup_contaplus_vm = import ./dotfiles/scripts/backup_vm_borg.nix {
      inherit pkgs;
      vm_name = "Contaplus";
      stop_vm = true;
    };
    # Direcline vm backup
    backup_direcline_vm = import ./dotfiles/scripts/backup_vm_borg.nix {
      inherit pkgs;
      vm_name = "Direcline";
      stop_vm = true;
    };
    # magatzem borgbackup repository summary
    borg_summary_magatzem = { passphrase }: import ./dotfiles/scripts/borgbackup_summary.nix {
      inherit pkgs passphrase;
      repository = "/var/lib/borgbackup/magatzem";
      account = "default";
      email = "informatica@trevol.com";
    };
    # contaplus borgbackup repository summary
    borg_summary_contaplus = { passphrase }: import ./dotfiles/scripts/borgbackup_summary.nix {
      inherit pkgs passphrase;
      repository = "/var/lib/borgbackup/contaplus";
      account = "default";
      email = "informatica@trevol.com";
    };
    # direcline borgbackup repository summary
    borg_summary_direcline = { passphrase }: import ./dotfiles/scripts/borgbackup_summary.nix {
      inherit pkgs passphrase;
      repository = "/var/lib/borgbackup/direcline";
      account = "default";
      email = "informatica@trevol.com";
    };

  };

  ssh.config = import ./dotfiles/ssh/config.nix;

}
