# Trèvol security configuration

{ network }:

{

  nixpkgs.config = {
    allowUnfree = true;
    permittedInsecurePackages = [ "googleearth-pro-7.3.4.8248" ];
  };

  security = {

    sudo = {
      enable = true;
      wheelNeedsPassword = false;
      extraConfig = ''
        Defaults logfile=/var/log/sudo
      '';
    };

  };

  programs.ssh = {
    startAgent = true;
    agentTimeout = null;
    hostKeyAlgorithms = [ "ssh-ed25519" "ssh-rsa" ];
    pubkeyAcceptedKeyTypes = [ "ssh-ed25519" "ssh-rsa" ];
  };

  services.openssh = {
    enable = true;
    allowSFTP = true;
    sftpFlags = [ ];
    ports = network.sshPorts;
    openFirewall = true;
    # see https://stribika.github.io/2015/01/04/secure-secure-shell.html
    # and https://infosec.mozilla.org/guidelines/openssh
    hostKeys = [
      { type = "rsa"; bits = 8192; path = "/etc/ssh/ssh_host_rsa_key"; rounds = 100; openSSHFormat = true; }
      { type = "ed25519"; bits = 8192; path = "/etc/ssh/ssh_host_ed25519_key"; rounds = 100; }
    ];
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      PermitRootLogin = "no";
      # kexAlgorithms, ciphers and macs
      KexAlgorithms = [
        "curve25519-sha256@libssh.org"
        "diffie-hellman-group-exchange-sha256"
      ];
      Ciphers = [
        "chacha20-poly1305@openssh.com"
      ];
      Macs = [
        "hmac-sha2-512-etm@openssh.com"
        "hmac-sha2-256-etm@openssh.com"
        "umac-128-etm@openssh.com"
        "hmac-sha2-512"
        "hmac-sha2-256"
        "umac-128@openssh.com"
      ];
    };
    # Allow use of ~/.ssh/environment
    extraConfig = ''
      PermitUserEnvironment yes
      AllowGroups marti_admin trevol borg
    '';
  };

  nix = {
    settings = {
      sandbox = true;
      trusted-substituters = network.nixosBinaryCaches;
      trusted-public-keys = network.binaryCachePublicKeys;
    };
  };
  boot.readOnlyNixStore = true;

}
