# Trèvol's organization-wide configuration

{ shared, lib, revision, paths, imports }:

rec {

  inherit revision;

  localization = import ./localization.nix;

  network = import ./network.nix {
    shared = shared.network;
  };

  security = import ./security.nix {
    network = network.defaults;
  };

  nix = import ./nix.nix {
    inherit lib;
    outPaths = paths;
  };

  _dotfiles = { pkgs }: import ./dotfiles.nix { inherit pkgs lib; };

  users = { pkgs, config, ... }: lib.recursive_merge [
    (shared._users { inherit pkgs; })
    (import ./users.nix {
      inherit lib pkgs config localization;
      dotfiles = _dotfiles { inherit pkgs; };
    })
  ];

  groups = lib.recursive_merge [
    shared.groups
    (import ./groups.nix)
  ];

  # modules to be imported by this organization's machines
  modules = [
    shared.nixosModule
    imports.dotfiles-marti.nixosModule
    imports.agenix.nixosModules.default
    nix
    security
    users
    groups
  ];

}
