# ssh keys used by agenix
let

  # users

  u_marti-casa = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAK7PSl4xpvZgAD2HstjuuyH0+5hCMV74a/oW7jgrB+a xvapx@xvapx";
  u_marti-trevol = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPyuZr+MtN7eOO9V0gdZ9dwHKPhdwr2+HAK4DIDLllS9 informatica@trevol.com";
  u_marti = [ u_marti-casa u_marti-trevol ];

  users = u_marti;

  # machines

  m_informatica = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJlBDXofJnsHLMdE0uBFd43xBh9PVW8ESKeqiR1DEmtC root@informatica";
  m_servidor = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILjaUSqdbaHYx1MnRuJTcTw6pm0yats3m3AKeENhUHE7 root@servidor-linux";

  m_marti = [ m_informatica ];
  m_trevol = [ m_informatica m_servidor ];

  hosts = m_trevol;

in {
  # marti google drive id and key
  "marti_gdrive_id.age".publicKeys = u_marti ++ m_marti;
  "marti_gdrive_key.age".publicKeys = u_marti ++ m_marti;
  # marti smb credentials
  "marti_smb_servidor.age".publicKeys = u_marti ++ m_trevol;
  # borgbackup magatzem pass and ssh keys
  "borgbackup_magatzem.age".publicKeys = users ++ m_trevol;
  "borgbackup_magatzem_ssh.age".publicKeys = users ++ m_trevol;
  "borgbackup_magatzem_ssh_pub.age".publicKeys = users ++ m_trevol;
  # borgbackup contaplus pass and ssh keys
  "borgbackup_contaplus.age".publicKeys = users ++ m_trevol;
  "borgbackup_contaplus_ssh.age".publicKeys = users ++ m_trevol;
  "borgbackup_contaplus_ssh_pub.age".publicKeys = users ++ m_trevol;
  # borgbackup direcline pass and ssh keys
  "borgbackup_direcline.age".publicKeys = users ++ m_trevol;
  "borgbackup_direcline_ssh.age".publicKeys = users ++ m_trevol;
  "borgbackup_direcline_ssh_pub.age".publicKeys = users ++ m_trevol;
  # email pass
  "informatica_pass.age".publicKeys = u_marti ++ m_marti;
  "alertes_pass.age".publicKeys = users ++ m_trevol;
  # nginx auth_basic_user_file
  "hydra_nginx_auth.age".publicKeys = users ++ m_trevol;
}
