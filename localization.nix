# Trèvol localization configuration

let
  # generate locales with ES keyboard
  machine_ES_locales = lang: loc: rec {

    # machine-wide settings
    i18n = {
      defaultLocale = "${lang}_${loc}.UTF-8";
      supportedLocales = [ "all" ];
    };
    time.timeZone = "Europe/Madrid";

    console = {
      keyMap = "es";
      font = "lat0-16";
    };

    services.xserver = {
      layout = "es";
      xkbOptions = "eurosign:e";
    };

    nixpkgs.config.i18n = with i18n; { inherit defaultlocale supportedLocales; };
  };

  # user settings
  user_locales = lang: loc: languages: { export ? false, plasma ? false }:
    let
      exp = if export then "export " else "";
      locale = "${lang}_${loc}.UTF-8";
    in
    (if plasma then ''
      [Formats]
    '' else "") +
    ''
      ${exp}LANG=${locale}
      ${exp}LANGUAGE=${languages}
      ${exp}LC_ADDRESS=${locale}
      ${exp}LC_COLLATE=${locale}
      ${exp}LC_CTYPE=${locale}
      ${exp}LC_IDENTIFICATION=${locale}
      ${exp}LC_MEASUREMENT=${locale}
      ${exp}LC_MESSAGES=${locale}
      ${exp}LC_MONETARY=${locale}
      ${exp}LC_NAME=${locale}
      ${exp}LC_NUMERIC=${locale}
      ${exp}LC_PAPER=${locale}
      ${exp}LC_TELEPHONE=${locale}
      ${exp}LC_TIME=${locale}
    '' +
    (if plasma then ''
      useDetailed=true

      [Translations]
      LANGUAGE=${lang}_${loc}
    '' else "");


in
{
  machine = {
    en_GB-es = machine_ES_locales "en" "GB";
    ca_ES-es = machine_ES_locales "ca" "ES";
    es_ES-es = machine_ES_locales "es" "ES";
  };
  user = {
    bash = {
      en = user_locales "en" "GB" "en" { export = true; };
      ca = user_locales "ca" "ES" "ca:es:en" { export = true; };
      es = user_locales "es" "ES" "es:ca:en" { export = true; };
    };
    plasma = {
      en = user_locales "en" "GB" "en" { plasma = true; };
      ca = user_locales "ca" "ES" "ca:es:en" { plasma = true; };
      es = user_locales "es" "ES" "es:ca:en" { plasma = true; };
    };
  };
}

