Trèvol configuration
============================
Trèvol systems managed using nix.

### This project installs an admin user with ssh acces keys and sudo capabilities, this is probably not what you want ###

### This project is still under heavy development, use at your own risk ###

How to use
----------

### Install:

0. Install NixOS on the new machine.

1. Create the `machines/${machine}.nix`, `machines/${machine}-software.nix`
and `machines/${machine}-hardware.nix`, and add the machine to `network.nix` and `flake.nix`.  
The 'nixosConfigurations' name in `flake.nix` **should** be the same as the hostname.

2. Clone this repository to `/etc/nixos`.

3. Rebuild the system with `nixos-rebuild switch --flake`.  
Use `nixos-rebuild switch --flake '#hostname' if the hostname is not the same as in flake.nix`.

4. Change the owner of `/etc/nixos` with
`chown -R marti_admin:marti_admin /etc/nixos`.

### Update (as marti_admin user):

1. `cd /etc/nixos`.

2. `git fetch && git pull`.

3. `sudo nixos-rebuild switch --flake`.


How to hack
------------

- Clone the repository
- Make the desired changes
- Build 'machine' in a vm with `nixos-rebuild build-vm --flake '.#machine'`
